# timetracker-backend

The backend for `timetracker` written with Python and Flask.

## Frontend

The corresponding frontend can be found [here](https://gitlab.com/freakout/timetracker-frontend).
