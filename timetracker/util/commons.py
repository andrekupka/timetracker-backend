def constant_time_compare(a, b):
    if len(a) != len(b):
        return False

    result = True
    for x, y in zip(a, b):
        if x != y:
            result = False
    return result
