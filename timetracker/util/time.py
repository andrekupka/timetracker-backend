import pytz
from datetime import datetime, timedelta
from flask import g


def get_utc_week_base():
    return datetime(1970, 1, 5, tzinfo=pytz.utc)

def get_week_start(id):
    base = get_utc_week_base()
    base += timedelta(days=7*id)
    localized = base.astimezone(g.user.get_timezone())
    localized -= localized.utcoffset()
    return localized.astimezone(pytz.utc)


def get_week_end(id):
    return get_week_start(id+1)

def get_month_start(id):
    year = 1970 + (id // 12)
    month = (id % 12) + 1
    return datetime(year, month, 1, tzinfo=pytz.utc)

def get_month_end(id):
    return get_month_start(id+1)
