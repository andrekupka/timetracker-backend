from flask import g
from flask.ext.restful import Resource

from . import api, is_authenticated
from ..model import Week


class WeekApi(Resource):
    @is_authenticated
    def get(self, week_id):
        return Week(g.user, week_id).serialize()


api.add_resource(WeekApi, "/weeks/<int:week_id>", endpoint="week")
