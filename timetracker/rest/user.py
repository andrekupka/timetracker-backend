from flask import g
from flask.ext.restful import abort, Resource

from ..model import User
from . import api, is_authenticated


class UserApi(Resource):
    @is_authenticated
    def get(self, user_id):
        user = User.load(user_id)
        if user == g.user:
            return user.serialize()
        abort(404)


api.add_resource(UserApi, "/user/<int:user_id>", endpoint="user")
