from flask.ext.restful import Api

from .. import app


api = Api(app, prefix="/api", catch_all_404s=True)


from .authentication import is_authenticated
from .util import abort, use_schema, use_kw_schema

import timetracker.rest.apis
import timetracker.rest.authentication
