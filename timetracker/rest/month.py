from flask import g
from flask.ext.restful import Resource

from . import api, is_authenticated
from ..model import Month


class MonthApi(Resource):
    @is_authenticated
    def get(self, month_id):
        return Month(g.user, month_id).serialize()


api.add_resource(MonthApi, "/months/<int:month_id>", endpoint="month")
