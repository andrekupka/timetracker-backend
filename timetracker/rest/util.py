from flask import request
import flask.ext.restful as restful


def use_schema(schema_class):
    schema = schema_class()
    def _decorator(f):
        def _inner(api, *args, **argv):
            data = schema.loads(request.data).data
            if "id" in data:
                del data["id"]
            return f(api, data, *args, **argv)
        return _inner
    return _decorator


def use_kw_schema(schema_class):
    schema = schema_class()
    def _decorator(f):
        def _inner(api, *args, **argv):
            data = schema.loads(request.data).data
            if "id" in data:
                del data["id"]
            data.update(argv)
            return f(api, *args, **data)
        return _inner
    return _decorator


def abort(code, message, *args, **kwargs):
    restful.abort(code, message=message.format(*args), **kwargs)
