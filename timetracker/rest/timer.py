from flask import g
from flask.ext.restful import abort, Resource

from ..model import Timer, TimerSchema
from . import api, is_authenticated, use_kw_schema


class TimerApi(Resource):
    @is_authenticated
    def get(self):
        timer = g.user.get_timer()
        if not timer:
            abort(404)
        return timer.serialize()

    @is_authenticated
    @use_kw_schema(TimerSchema)
    def put(self, timer_start):
        g.user.set_timer_start(timer_start)
        g.user.save()
        return g.user.get_timer().serialize()

    @is_authenticated
    def delete(self):
        g.user.set_timer_start(None)
        g.user.save()
        return '', 204


api.add_resource(TimerApi, "/timer", endpoint="timer")
