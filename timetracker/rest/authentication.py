from flask import g, request
from functools import wraps
from itsdangerous import BadSignature, SignatureExpired

from .. import app
from ..model import Token


from .util import abort


def is_authenticated(f):
    @wraps(f)
    def _inner(*args, **kwargs):
        if g.user:
            return f(*args, **kwargs)
        abort(401, "Not authenticated")
    return _inner


@app.before_request
def check_authentication_token():
    authentication = request.headers.get('Authentication')
    if not authentication:
        g.user = None
        return

    try:
        g.user = Token.get_user(authentication)
    except (BadSignature, ValueError):
        abort(401, "Bad Signature")
    except SignatureExpired:
        abort(401, "Signature Expired")
