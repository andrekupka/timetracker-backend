from flask.ext.restful import Resource

from . import api, is_authenticated


class LogoutApi(Resource):
    @is_authenticated
    def post(self):
        return '', 204


api.add_resource(LogoutApi, "/logout", endpoint="logout")
