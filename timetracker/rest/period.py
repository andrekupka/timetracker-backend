from flask import g
from flask.ext.restful import Resource

from ..model import Period, PeriodSchema
from . import api, is_authenticated, use_schema


class PeriodListApi(Resource):
    @is_authenticated
    def get(self):
        periods = Period.load_all()
        return Period.serialize_all(periods)

    @is_authenticated
    @use_schema(PeriodSchema)
    def post(self, args):
        period = Period(user=g.user, **args)
        period.save()
        return period.serialize(), 201


class PeriodApi(Resource):
    @is_authenticated
    def get(self, period_id):
        period = Period.load(period_id)
        return period.serialize()

    @is_authenticated
    @use_schema(PeriodSchema)
    def patch(self, args, period_id):
        period = Period.load(period_id)
        period.update(args)
        return period.serialize()

    @is_authenticated
    def delete(self, period_id):
        Period.delete_by_id(period_id)
        return "", 204


api.add_resource(PeriodListApi, "/periods", endpoint="periods")
api.add_resource(PeriodApi, "/periods/<int:period_id>", endpoint="period")
