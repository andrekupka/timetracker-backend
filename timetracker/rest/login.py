from flask.ext.restful import Resource
from webargs import fields
from webargs.flaskparser import use_kwargs

from ..model import Token, User
from . import abort, api


class LoginApi(Resource):
    POST_ARGS = dict(
        username=fields.String(required=True),
        password=fields.String(required=True)
    )

    @use_kwargs(POST_ARGS)
    def post(self, username, password):
        user = User.load_by_name(username, no404=True)
        if not user or not user.is_password_valid(password):
            abort(401, "Incorrect username or password", username,
                  error="Invalid")
        return Token(user).serialize()


api.add_resource(LoginApi, "/login", endpoint="login")
