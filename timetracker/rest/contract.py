from flask import g
from flask.ext.restful import Resource

from ..model import Contract, ContractSchema
from . import api, is_authenticated, use_schema


class ContractListApi(Resource):
    @is_authenticated
    def get(self):
        contracts = Contract.load_all()
        return Contract.serialize_all(contracts)

    @is_authenticated
    @use_schema(ContractSchema)
    def post(self, args):
        contract = Contract(user=g.user, **args)
        contract.save()
        return contract.serialize(), 201


class ContractApi(Resource):
    @is_authenticated
    def get(self, contract_id):
        contract = Contract.load(contract_id)
        return contract.serialize()

    @is_authenticated
    @use_schema(ContractSchema)
    def patch(self, args, contract_id):
        contract = Contract.load(contract_id)
        contract.update(args)
        return contract.serialize()

    @is_authenticated
    def delete(self, contract_id):
        Contract.delete_by_id(contract_id)
        return "", 204


api.add_resource(ContractListApi, "/contracts", endpoint="contracts")
api.add_resource(ContractApi, "/contracts/<int:contract_id>",
                 endpoint="contract")
