from datetime import datetime

from .. import db
from .exception import handle_database_error
from .serialization import Serializable
from .util import is_valid_id


class Resource(db.Model, Serializable):
    __abstract__ = True

    def __init__(self, **kwargs):
        forward_args = dict()
        for key, value in kwargs.iteritems():
            if self.is_relationship(key) and is_valid_id(value):
                self.update_relationship(key, value)
            else:
                forward_args[key] = value
        super(Resource, self).__init__(**forward_args)

    def update(self, args, commit=True):
        for key, value in args.iteritems():
            if self.is_relationship(key) and is_valid_id(value):
                self.update_relationship(key, value)
            else:
                if isinstance(value, datetime):
                    value = value.replace(tzinfo=None)
                if value is not None:
                    setattr(self, key, value)
        self.save(commit)

    def update_relationship(self, key, value):
        relation_class = self.get_relationship_class(key)
        if isinstance(value, list):
            int_values = [int(x) for x in value]
            self.update_many_relationship(relation_class, key, int_values)
        else:
            self.update_single_relationship(relation_class, key, int(value))

    def update_single_relationship(self, relation_class, key, value):
        setattr(self, key, relation_class.load(value))

    def update_many_relationship(self, relation_class, key, values):
        raise NotImplementedError("Updating a many relationship is not" +
                                  "supported yet")

    def is_relationship(self, key):
        return key in self.__mapper__.relationships.keys()

    def get_relationship_class(self, key):
        return self.__mapper__.relationships[key].mapper.class_

    def save(self, commit=True):
        db.session.add(self)
        if commit:
            db.session.commit()

    def delete(self, commit=True):
        db.session.delete(self)
        if commit:
            db.session.commit()

    @classmethod
    def load_all_ids(cls, ids):
        return cls.query.filter(cls.id.in_(ids)).all()

    @classmethod
    def load_all(cls):
        return cls.query.all()

    @classmethod
    def load(cls, id):
        return cls.query.get_or_404(id)

    @classmethod
    @handle_database_error
    def delete_by_id(cls, id, commit=True):
        resource = cls.query.get(id)
        if resource:
            resource.delete(commit)

    def __eq__(self, other):
        if other is None or not isinstance(other, type(self)):
            return False
        return self.id == other.id

    def __ne__(self, other):
        if other is None or not isinstance(other, type(self)):
            return True
        return self.id != other.id
