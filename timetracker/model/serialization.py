class Serializable(object):
    __schema__ = None

    def serialize(self, schema=None):
        schema = schema or self.__schema__
        return schema.dump(self).data

    @classmethod
    def serialize_all(cls, data, schema=None):
        schema = schema or cls.__schema__
        return schema.dump(data, many=True).data
