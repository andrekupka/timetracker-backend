from flask.ext.restful import abort

from marshmallow_jsonapi import fields, Schema
from marshmallow_jsonapi.flask import Relationship

from .. import db
from .userresource import UserResource


class PeriodSchema(Schema):
    id = fields.Integer(dump_only=True)
    from_time = fields.DateTime()
    to_time = fields.DateTime()
    comment = fields.String()
    contract = Relationship(related_view="contracts", many=False,
                            include_data=True, type_="contracts")

    class Meta:
        type_ = "periods"
        strict = True


class Period(UserResource):
    __schema__ = PeriodSchema()

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.ForeignKey("user.id"), nullable=False)
    from_time = db.Column(db.DateTime(), nullable=False)
    to_time = db.Column(db.DateTime(), nullable=False)
    comment = db.Column(db.UnicodeText())
    contract_id = db.Column(db.ForeignKey("contract.id"), nullable=False)

    def __init__(self, from_time, to_time, *args, **kwargs):
        if to_time <= from_time:
            raise ValueError("to_time must be greater than from_time")
        self.to_time = to_time
        self.from_time = from_time
        super(Period, self).__init__(*args, **kwargs)
