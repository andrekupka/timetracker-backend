def is_valid_id(value):
    try:
        if isinstance(value, list):
            value = [int(x) for x in value]
        else:
            value = int(value)
    except (ValueError, TypeError):
        return False
    return True
