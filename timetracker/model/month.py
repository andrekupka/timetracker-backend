from .interval import Interval, IntervalSchema
from ..util.time import get_month_start, get_month_end


class MonthSchema(IntervalSchema):

    class Meta:
        type_ = 'months'
        strict = True


class Month(Interval):
    __schema__ = MonthSchema()

    def __init__(self, user, id):
        super(Month, self).__init__(user, id)

    def compute_start_time(self, id):
        return get_month_start(id)

    def compute_end_time(self, id):
        return get_month_end(id)
