from .interval import Interval, IntervalSchema
from ..util.time import get_week_start, get_week_end


class WeekSchema(IntervalSchema):

    class Meta:
        type_ = 'weeks'
        strict = True


class Week(Interval):
    __schema__ = WeekSchema()

    def __init__(self, user, id):
        super(Week, self).__init__(user, id)

    def compute_start_time(self, id):
        return get_week_start(id)

    def compute_end_time(self, id):
        return get_week_end(id)
