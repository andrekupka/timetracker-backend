from flask.ext.restful import abort
from sqlalchemy.exc import IntegrityError

from functools import wraps


def handle_database_error(f):
    @wraps(f)
    def _inner(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except IntegrityError as e:
            abort(409, message=str(e))
    return _inner
