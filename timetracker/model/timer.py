from marshmallow import fields, Schema

from .serialization import Serializable


class TimerSchema(Schema):
    timer_start = fields.DateTime()


class Timer(Serializable):
    __schema__ = TimerSchema()

    def __init__(self, user):
        self.timer_start = user.timer_start
