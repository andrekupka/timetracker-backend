from marshmallow_jsonapi import fields, Schema
from marshmallow_jsonapi.flask import Relationship

from .. import db
from .userresource import UserResource


class ContractSchema(Schema):
    id = fields.Integer()
    name = fields.String()
    hours_per_week = fields.Integer()
    start_date = fields.DateTime()
    end_date = fields.DateTime()
    periods = Relationship(related_view="periods", many=True,
                           include_data=True, type_='periods')

    class Meta:
        type_ = "contracts"
        strict = True


class Contract(UserResource):
    __schema__ = ContractSchema()

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.ForeignKey("user.id"), nullable=False)
    name = db.Column(db.UnicodeText, nullable=False, unique=True)
    hours_per_week = db.Column(db.Integer, nullable=False)
    start_date = db.Column(db.DateTime(), nullable=False)
    end_date = db.Column(db.DateTime(), nullable = False)
    periods = db.relationship("Period", backref="contract", lazy="dynamic",
                              cascade="all")

    def __init__(self, start_date, end_date, *args, **kwargs):
        if end_date < start_date:
            raise ValueError("end_date must be greater or equal to start_date")
        self.start_date = start_date
        self.end_date = end_date
        super(Contract, self).__init__(*args, **kwargs)
