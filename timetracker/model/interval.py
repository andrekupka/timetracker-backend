from abc import ABCMeta, abstractmethod
from marshmallow_jsonapi import fields, Schema
from marshmallow_jsonapi.flask import Relationship
from sqlalchemy import and_, or_

from .period import Period
from .serialization import Serializable


class IntervalSchema(Schema):
    id = fields.Integer()
    start_time = fields.DateTime()
    end_time = fields.DateTime()
    periods = Relationship(related_view="periods", many=True,
                           include_data=True, type_="periods")


class Interval(Serializable):
    __metaclass__ = ABCMeta

    def __init__(self, user, id):
        self.user = user
        self.id = id
        self.start_time = self.compute_start_time(id)
        self.end_time = self.compute_end_time(id)
        self.periods = self.compute_periods()

    @abstractmethod
    def compute_start_time(self, id):
        pass

    @abstractmethod
    def compute_end_time(self, id):
        pass

    def get_start_time(self):
        return self.start_time

    def get_end_time(self):
        return self.end_time

    def compute_periods(self):
        filters = [self.user.id == Period.user_id,
                   or_(and_(Period.from_time >= self.get_start_time(),
                            Period.from_time < self.get_end_time()),
                       and_(Period.to_time > self.get_start_time(),
                            Period.to_time <= self.get_end_time()))]
        return Period.query.filter(*filters).all()
