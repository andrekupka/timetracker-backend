from flask import g

from .resource import Resource


class UserResource(Resource):
    __abstract__ = True

    @classmethod
    def load_all_ids(cls, ids):
        objs = cls.query.filter(cls.id.in_(ids)).all()
        for obj in objs:
            if obj.user != g.user:
                abort(404)
        return obj


    @classmethod
    def load_all(cls):
        return cls.query.filter_by(user_id=g.user.id).all()

    @classmethod
    def load(cls, id):
        obj = cls.query.get_or_404(id)
        if obj.user != g.user:
            abort(404)
        return obj
