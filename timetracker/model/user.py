import hashlib
import os
import pytz

from marshmallow_jsonapi import fields, Schema

from .. import db
from ..util import constant_time_compare
from .resource import Resource
from .timer import Timer


class UserSchema(Schema):
    id = fields.Integer()
    username = fields.String()
    full_name = fields.String()
    timezone = fields.String()

    class Meta:
        type_ = "users"
        strict = True


class User(Resource):
    __schema__ = UserSchema()

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True,
                         nullable=False)
    full_name = db.Column(db.Unicode(128), nullable=False)
    password_hash = db.Column(db.String(64), nullable=False)
    salt = db.Column(db.String(128), nullable=False)
    timezone = db.Column(db.String(64))
    timer_start = db.Column(db.DateTime())
    contracts = db.relationship("Contract", backref="user", lazy="dynamic")
    periods = db.relationship("Period", backref="user", lazy="dynamic")

    def __init__(self, username, full_name, password, timezone):
        super(User, self).__init__()
        self.username = username
        self.full_name = full_name
        self.salt = os.urandom(64).encode("hex")
        self.password_hash = self._calculate_hash(password)
        self.timezone = timezone
        self.timer_start = None

    def _calculate_hash(self, password):
        h = hashlib.sha256()
        h.update(self.salt + password)
        return h.hexdigest()

    def is_password_valid(self, password):
        other_hash = self._calculate_hash(password)
        return constant_time_compare(other_hash, self.password_hash)

    def get_timezone(self):
        if not self.timezone:
            return pytz.utc()
        return pytz.timezone(self.timezone)

    def localize(self, date_time):
        return self.get_timezone().localize(date_time, is_dst=False)

    def get_timer(self):
        if not self.timer_start:
            return None
        return Timer(self)

    def set_timer_start(self, timer_start):
        if timer_start:
            timer_start = timer_start.replace(tzinfo=None)
        self.timer_start = timer_start

    @classmethod
    def load_by_name(cls, username, no404=False):
        query = cls.query.filter_by(username=username)
        if no404:
            return query.first()
        return query.first_or_404()

    def __repr__(self):
        return "<User {}>".format(self.username)
