from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from marshmallow import fields, Schema

from .. import app
from .user import User
from .serialization import Serializable


class PlainUserSchema(Schema):
    id = fields.Integer()
    username = fields.String()
    fullName = fields.Function(lambda obj: obj.full_name)


class TokenSchema(Schema):
    token = fields.String()
    user = fields.Nested(PlainUserSchema)


class Token(Serializable):
    __schema__ = TokenSchema()

    DEFAULT_EXPIRATION = 60 * 60 * 24

    def __init__(self, user, expiration=DEFAULT_EXPIRATION):
        serializer = Serializer(app.config["SECRET_KEY"],
                                expires_in=expiration)
        self.token = serializer.dumps(dict(id=user.id))
        self.user = user

    @staticmethod
    def get_user(token):
        serializer = Serializer(app.config["SECRET_KEY"])
        data = serializer.loads(token)
        if "id" not in data:
            raise ValueError("Token does not contain user id")
        return User.load(data["id"])
