#!/usr/bin/env python2

import sys

from datetime import date, datetime, timedelta

from timetracker import db
from timetracker.model import Period, User, Contract


def usage():
    print("Please specify an user password. Additionally you can specify a"
          "username which will be the default user")
    sys.exit(1)


def main():
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        usage()

    password = sys.argv[1]
    if len(sys.argv) == 3:
        username = sys.argv[2].decode('utf-8')
        create_user(username, password)
        return

    user1, user2 = create_users(password)
    contract = create_contract(user1)
    create_periods(user1, contract)


def create_user(username, password):
    User.query.delete()
    user = User(username=username, full_name=username,
                password=password, timezone="Europe/Berlin")
    user.save()

def create_users(password):
    User.query.delete()
    user1 = User(username=u"freakout", full_name=u"Andre Kupka",
                 password=password, timezone="Europe/Berlin")
    user2 = User(username=u"christine", full_name=u"Christine Kupka",
                 password=password, timezone="Europe/Berlin")
    user1.save()
    user2.save()
    return user1, user2


def create_contract(user):
    Contract.query.delete()
    start_date = date.today() - timedelta(days=3)
    end_date = date.today() + timedelta(days=4)
    contract = Contract(user=user, name=u"Test Name",
                        hours_per_week=30, start_date=start_date,
                        end_date=end_date)
    contract.save()
    return contract


def create_periods(user, contract):
    Period.query.delete()

    from_time = datetime.utcnow()
    to_time = from_time + timedelta(hours=2)
    period1 = Period(user=user, contract=contract, from_time=from_time,
                     to_time=to_time, comment=u"User 1, First")

    from_time = to_time + timedelta(hours=1)
    to_time = from_time + timedelta(hours=3)
    period2 = Period(user=user, contract=contract, from_time=from_time,
                     to_time=to_time, comment=u"User 1, Second")

    period1.save()
    period2.save()


if __name__ == "__main__":
    main()
